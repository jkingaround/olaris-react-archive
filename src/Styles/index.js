import styled from 'styled-components';

export const PageHeading = styled.h1`
    color: #fff;
    font-size: 3.2rem;
    font-weight: 300;
    letter-spacing: 0.2rem;
    margin: 0 0 3rem;
`;

export const PageSubTitle = styled.p`
    font-size: 1.4rem;
`;
